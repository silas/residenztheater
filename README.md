# Discontinued! Residenztheater does not provide monthly calendars any more!

# Residenztheater - Kalender

Das Münchner Residenztheater bietet Kalender nur per Monat an. Dieses Skript bastelt einen mehrmonatigen Kalender (soweit verfügbar) daraus.

## Benutzung

./resical NameDerAusgabeDatei.ics

# Residenztheater - Calendar

The Munich Residenztheater offers .ics calendars only per month. This script assemblys a unified multi-month calendar, as available.

## Usage

./resical OuputFile.ics